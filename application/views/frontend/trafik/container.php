<script> 
	var chart;
	var chart2;
	var chart3;
	function requestDatta(interface) {
		$.ajax({
			//url: 'data.php?interface='+interface,
			url: '<?=base_url('frontend/trafik/grafik/')?>'+interface,
			datatype: "json",
			success: function(data) {
				var midata = JSON.parse(data);
				if( midata.length > 0 ) {
					var TX=parseFloat(midata[0].data);
					var RX=parseFloat(midata[1].data);
					var x = (new Date()).getTime(); 
					shift=chart.series[0].data.length > 19;
					chart.series[0].addPoint([x, TX], true, shift);
					chart.series[1].addPoint([x, RX], true, shift);
					document.getElementById("trafico").innerHTML="<span class='text-green'><span class='fa fa-arrow-down'></span> Download : "+RX + " Mbps </span>/ <span class='text-red'><span class='fa fa-arrow-up'></span> Upload : " + TX +" Mbps </span>";
				}else{
					document.getElementById("trafico").innerHTML="- / -";
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				console.error("Status: " + textStatus + " request: " + XMLHttpRequest); console.error("Error: " + errorThrown); 
			}       
		});
	}	
	function requestDatta2(interface) {
		$.ajax({
			//url: 'data.php?interface='+interface,
			url: '<?=base_url('frontend/trafik/grafik/')?>'+interface,
			datatype: "json",
			success: function(data) {
				var midata = JSON.parse(data);
				if( midata.length > 0 ) {
					var TX=parseFloat(midata[0].data);
					var RX=parseFloat(midata[1].data);
					var x = (new Date()).getTime(); 
					shift=chart2.series[0].data.length > 19;
					chart2.series[0].addPoint([x, TX], true, shift);
					chart2.series[1].addPoint([x, RX], true, shift);
					document.getElementById("trafico2").innerHTML="<span class='text-green'><span class='fa fa-arrow-down'></span> Download : "+TX + " Mbps </span>/ <span class='text-red'><span class='fa fa-arrow-up'></span> Upload : " + RX +" Mbps </span>";
					//document.getElementById("trafico2").innerHTML=midata[1].data;
				}else{
					document.getElementById("trafico").innerHTML="- / -";
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				console.error("Status: " + textStatus + " request: " + XMLHttpRequest); console.error("Error: " + errorThrown); 
			}       
		});
	}
	function requestDatta3(interface) {
		$.ajax({
			//url: 'data.php?interface='+interface,
			url: '<?=base_url('frontend/trafik/resource')?>',
			datatype: "json",
			success: function(data) {
				var midata = JSON.parse(data);
				if( midata.length > 0 ) {
					document.getElementById("uptime").innerHTML=midata[0].uptime;
					document.getElementById("freehd").innerHTML=midata[0].freehdd;
					document.getElementById("usedmemory").innerHTML=midata[0].usedmemory;
					document.getElementById("cpuload").innerHTML=midata[0].cpuload;
					document.getElementById("informasiro").innerHTML=midata[0].boardname;
				}else{
					//document.getElementById("keterangan").innerHTML="- / -";
					//document.getElementById("keterangan").innerHTML=midata[0].uptime;
				}

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				console.error("Status: " + textStatus + " request: " + XMLHttpRequest); console.error("Error: " + errorThrown); 
			}       
		});
	}	
	$(document).ready(function() {
			Highcharts.setOptions({
				global: {
					useUTC: false
				}
			});		
           chart= new Highcharts.Chart({
			   chart: {
				renderTo: 'container',
				animation: Highcharts.svg,
				//type: 'spline',
				type: 'area',
				events: {
					load: function () {
						setInterval(function () {
							requestDatta(document.getElementById("interface").value);
						}, 1000);
					}				
				}
		 		},
				 title: {
					text: 'Grafik Interface'
				 },
				 xAxis: {
					type: 'datetime',
						tickPixelInterval: 150,
						maxZoom: 20 * 1000
				 },
				 yAxis: {
					minPadding: 0.2,
						maxPadding: 0.2,
						title: {
							text: 'Trafik',
							margin: 20
						}
				 },
		            series: [{
		                name: 'TX/Upload',
		                data: [],
		                //lineColor:"green"
		            }, {
		                name: 'RX/Download',
		                data: [],
		                //lineColor:"red"
		            }]
			});
           chart2 = new Highcharts.Chart({
			   chart: {
				renderTo: 'grafik',
				animation: Highcharts.svg,
				//type: 'spline',
				type: 'area',
				events: {
					load: function () {
						setInterval(function () {
							requestDatta2(document.getElementById("interface2").value);
						}, 1000);
					}				
				}
		 		},
				 title: {
					text: 'Grafik Interface'
				 },
				 xAxis: {
					type: 'datetime',
						tickPixelInterval: 150,
						maxZoom: 20 * 1000
				 },
				 yAxis: {
					minPadding: 0.2,
						maxPadding: 0.2,
						title: {
							text: 'Trafik',
							margin: 20
						}
				 },
		            series: [{
		                name: 'TX/Download',
		                data: [],
		                color:"rgb(0,128,0,0.5)",
		                
		                //lineColor:"rgb(255,0,0)",
		                //fillOpacity: 0.75,
		                
		            }, {
		                name: 'RX/Upload',
		                data: [],
		                color:"rgb(255,0,0,0.5)",
		                //lineColor:"red"
		            }]
			});
   //         chart3 = new Highcharts.Chart({
			//    chart: {
			// 	renderTo: 'grafik2',
			// 	animation: Highcharts.svg,
			// 	//type: 'spline',
			// 	type: 'area',
			// 	events: {
			// 		load: function () {
			// 			setInterval(function () {
			// 				requestDatta3(document.getElementById("interface2").value);
			// 			}, 1000);
			// 		}				
			// 	}
		 // 		},
			// 	 title: {
			// 		text: 'Grafik Interface'
			// 	 },
			// 	 xAxis: {
			// 		type: 'datetime',
			// 			tickPixelInterval: 150,
			// 			maxZoom: 20 * 1000
			// 	 },
			// 	 yAxis: {
			// 		minPadding: 0.2,
			// 			maxPadding: 0.2,
			// 			title: {
			// 				text: 'Trafik',
			// 				margin: 20
			// 			}
			// 	 },
		 //            series: [{
		 //                name: 'TX/Download',
		 //                data: [],
		 //                color:"rgb(0,128,0,0.5)",
		                
		 //                //lineColor:"rgb(255,0,0)",
		 //                //fillOpacity: 0.75,
		                
		 //            }, {
		 //                name: 'RX/Upload',
		 //                data: [],
		 //                color:"rgb(255,0,0,0.5)",
		 //                //lineColor:"red"
		 //            }]
			// });	

		
			setInterval(function () {
				requestDatta3(document.getElementById("interface").value);
			}, 1000);
		

  });
</script>
<div id="view">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<h3 style="margin: 0px">Router <span id="informasiro"></span></h3>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-5">
			<div class="row">
				<div class="col-sm-8">
					<div class="form-group">
						<label>Pilih Interface</label>
						<!--GUNAKAN UNDERSCOR '_' UNTUK SPASI-->
						<select id="interface" disabled class="form-control selectdata" style="width: 100%">
							<option value="ether1" selected="selected">Ether 1 INTERNET</option>
							<option value="ether2">Ether 2 VLAN LOKAL</option>
							
							<option value="vlan_777">Hotspot</option>
							<option value="vlan_888">Wifi Doktor</option>
							<option value="vlan_100">Basement</option>
							<option value="vlan_101">Lantai 1</option>
						</select>
					</div>		
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
				    <p>Trafik Realtime : <span id="trafico"></span></p>
					<div id="container" style="width: 100%; height: 400px; margin: 0 auto"></div>				
				</div>	
			</div>
		</div>
		<div class="col-sm-5">
			<div class="row">
				<div class="col-sm-8">
					<div class="form-group">
						<label>Pilih Interface</label>
						<!--GUNAKAN UNDERSCOR '_' UNTUK SPASI-->
						<select id="interface2" class="form-control selectdata" style="width: 100%">
							
							<option value="ether2">Ether 2 VLAN LOKAL</option>
							
							<option value="vlan_777">Hotspot</option>
							<option value="vlan_888">Wifi Doktor</option>
							<option value="vlan_100">Basement</option>
							<option value="vlan_101">Lantai 1</option>
						</select>
					</div>		
				</div>
			</div>			
			<div class="row">
				<div class="col-sm-12">
				    <p>Trafik Realtime : <span id="trafico2"></span></p>
					<div id="grafik" style="width: 100%; height: 400px; margin: 0 auto"></div>				
				</div>					
			</div>
		</div>
		<div class="col-sm-2">
			<div class="small-box bg-aqua"  style="height: 100px" \>
				<div class="inner">
					<h3 style="font-size: 15px;margin-bottom: "><span id="uptime">0</span></h3>
					<p>Uptime</p>
				</div>
				<div class="icon">
					<i class="fa fa-calendar" style="font-size: 32px"></i>
				</div>
			</div>
			<div class="small-box bg-green" style="height: 100px">
				<div class="inner">
					<h3 style="font-size: 24px"><span id="freehd">0</span><sup>Mb/1 Gb</sup></h3>
					<p>Free HD</p>
				</div>
				<div class="icon">
					<i class="fa fa-area-chart" style="font-size: 32px"></i>
				</div>
			</div>
			<div class="small-box bg-aqua" style="height: 100px">
				<div class="inner">
					<h3 style="font-size: 24px"><span id="usedmemory">0</span><sup>Mb/4 Gb</sup></h3>
					<p>Memori RAM</p>
				</div>
				<div class="icon">
					<i class="fa fa-area-chart" style="font-size: 32px"></i>
				</div>
			</div>
			<div class="small-box bg-green" style="height: 100px">
				<div class="inner">
					<h3 style="font-size: 24px"><span id="cpuload">0</span><sup>%</sup></h3>
					<p>CPU Load</p>
				</div>
				<div class="icon">
					<i class="fa  fa-tachometer" style="font-size: 32px"></i>
				</div>
			</div>												
		</div>
	</div>	
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	    setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 
</script>