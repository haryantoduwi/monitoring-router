<?php
////////////////////////////////////////////////////////////////////
// ESTE EJEMPLO SE DESCARGO DE www.tech-nico.com ///////////////////
// Creado por: Nicolas Daitsch. Guatrache. La Pampa ////////////////
// Contacto: administracion@tech-nico.com //////////////////////////
// RouterOS API: Grafica en tiempo real usando highcharts //////////
//////////////////////////////////////////////////////////////////// ?>
<?php require_once('api_mikrotik.php'); ?>
<?php
$ipRouteros = "192.168.50.1";
$Username="admin";
$Pass="rsuii";
$api_puerto=8728;
$interface = $_GET["interface"]; //"<pppoe-nombreusuario>";
//$interface = 'ether2';
	$API = new routeros_api();
	$API->debug = false;
	if ($API->connect($ipRouteros , $Username , $Pass, $api_puerto)) {
		$rows = array(); $rows2 = array();	
		   $API->write("/interface/monitor-traffic",false);
		   $API->write("=interface=".$interface,false);  
		   $API->write("=once=",true);
		   $READ = $API->read(false);
		   $ARRAY = $API->parse_response($READ);
			if(count($ARRAY)>0){  
				//$rx = number_format($ARRAY[0]["rx-bits-per-second"]/1024,1);
				$rx = number_format($ARRAY[0]["rx-bits-per-second"]/1024,0);
				$tx = number_format($ARRAY[0]["tx-bits-per-second"]/1024,0);

				//KONVERSI KE BILANGAN PECAHAN
				$rx=number_format(str_replace(',', '.', $rx),2);
				$tx=number_format(str_replace(',', '.', $tx),2);
				
				$rows['name'] = 'Tx';
				$rows['data'][] = $tx;
				$rows2['name'] = 'Rx';
				$rows2['data'][] = $rx;
			}else{  
				echo $ARRAY['!trap'][0]['message'];	 
			} 
	}else{
		echo "<font color='#ff0000'>Koneksi down/gagal, silahkan di cek ulang</font>";
	}
	$API->disconnect();

	$result = array();
	array_push($result,$rows);
	array_push($result,$rows2);
	print json_encode($result, JSON_NUMERIC_CHECK);

?>
