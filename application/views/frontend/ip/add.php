<div class="row">
	<div class="col-sm-12 animated bounceInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
				<button type="button" onclick="loaddata()" class="btn btn-xs pull-right btn-danger btn-flat"><i class="fa fa-arrow-left"></i> Kembali</button>
			</div>
			<div class="box-body">
				<form id="formadd" method="POST" action="javascript:void(0)" url="<?= base_url($global->url)?>"  enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Alias</label>
								<input type="text"  name="ip_alias" class="form-control"">
							</div>							
							<div class="form-group">
								<label>Ip Address</label>
								<input required type="text" name="ip_address" class="form-control" title="Harus di isi">
							</div>
							<div class="form-group">
								<label>Status</label>
				                  <div class="radio">
				                    <label>
				                      <input type="radio" name="ip_status" id="optionsRadios1" value="1" checked>
				                      Aktif
				                    </label>
				                    <label>
				                      <input type="radio" name="ip_status" id="optionsRadios1" value="0">
				                      Tidak Aktif
				                    </label>				                    
				                  </div>
							</div>																	 
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">						
							<div class="form-group">
								<button type="submit" value="submit" name="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
							</div>														
						</div>
					</div>
				</form>		
			</div>
		</div>		
	</div>	
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');
</script>
