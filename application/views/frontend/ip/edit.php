<div class="row">
	<div class="col-sm-12 animated bounceInRight">
		<div class="box box-warning">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
								<button type="button" onclick="loaddata()" class="btn btn-xs pull-right btn-danger btn-flat"><i class="fa fa-arrow-left"></i> Kembali</button>
			</div>
			<div class="box-body">
				<form id="formadd" method="POST"  action="javascript:void(0)" url="<?= base_url($global->url.'edit')?>" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Id</label>
								<input required readonly type="text" name="id" class="form-control" value="<?= $data->ip_id?>" title="Harus di isi">
							</div>						
							<div class="form-group">
								<label>Alias</label>
								<input type="text"  name="ip_alias" class="form-control"" value="<?= $data->ip_alias?>">
							</div>							
							<div class="form-group">
								<label>Ip Address</label>
								<input required type="text" name="ip_address" class="form-control" title="Harus di isi" value="<?= $data->ip_address?>">
							</div>
							<div class="form-group">
								<label>Status</label>
				                  <div class="radio">
				                    <label>
				                      <input type="radio" name="ip_status"  value="1" <?=$data->ip_status==1? 'checked':''?>>
				                      Aktif
				                    </label>
				                    <label>
				                      <input type="radio" name="ip_status"  value="0" <?=$data->ip_status==0? 'checked':''?>>
				                      Tidak Aktif
				                    </label>				                    
				                  </div>
							</div>	 
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">						
							<div class="form-group">
								<button type="submit" value="submit" name="submit" class="btn btn-warning btn-block btn-flat">Update</button>
							</div>														
						</div>
					</div>
				</form>		
			</div>
		</div>		
	</div>	
</div>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');	
</script>
<?php include 'action.php'?>