<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';
include APPPATH.'controllers/Api_mikrotik.php';

class Trafik extends Master {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');
		// if(($this->session->userdata('login')!=true) || ($this->session->userdata('level')!=1) ){
		// 	redirect(site_url('login/logout'));
		// }
	}
	//VARIABEL
	private $master_tabel="user"; //Mendefinisikan Nama Tabel
	private $id="user_id";	//Menedefinisaikan Nama Id Tabel
	private $default_url="frontend/trafik/"; //Mendefinisikan url controller
	private $default_view="frontend/trafik/"; //Mendefinisiakn defaul view
	private $view="template/webfrontend"; //Mendefinisikan Tamplate Root
	private $path='./upload/';

	// RUBAH DATA USER DISINI UTNUK ROUTER
	/////////////////////////////////////////
	private	$iprouter = "192.168.50.1";
	private $username="rsuii";
	private	$password="*r5u11";
	private	$api_port=8728;	
	////////////////////////////////////////

	private function global_set($data){
		$data=array(
			'menu'=>'monitoring',//Seting menu yang aktif
			'submenu_menu'=>$data['submenu'],
			'menu_submenu'=>false,
			'headline'=>$data['headline'], //Deskripsi Menu
			'url'=>$data['url'], //Deskripsi URL yang dilewatkan dari function
			'ikon'=>"fa fa-area-chart",
			'view'=>"views/frontend/trafik/container.php",
			'detail'=>false,
			'cetak'=>false,
			'edit'=>true,
			'delete'=>true,
		);
		return (object)$data; //MEMBUAT ARRAY DALAM BENTUK OBYEK
		//$data->menu=home, bentuk obyek
		//$data['menu']=home, array bentuk biasa
	}
	public function testtrafik2(){
		$perintah=$this->input->post('id');
		echo "<pre>";
		if($perintah){
			echo system('trafik -c 1 '.$perintah);
		}else{
			echo 'Tidak ada masukkan';
		}
				
	}
	public function index()
	{
		$global_set=array(
			'headline'=>'trafik',
			'url'=>$this->default_url,
			'submenu'=>'trafik',
		);
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			$data=array(
				'user_nama'=>$this->input->post('user_nama'),
				'user_terdaftar'=>date('Y-m-d',strtotime($this->input->post('user_terdaftar'))),
				'user_username'=>$this->input->post('user_username'),
				'user_password'=>$this->input->post('user_password'),
				'user_level'=>$this->input->post('user_level'),
			);
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
			//print_r($data);
		}else{
			$ip=array(
				'tabel'=>'ip',
				'where'=>array(array('ip_status'=>1)),
			);
			$data=array(
				'global'=>$global,
				'menu'=>$this->menu(0),
				'ip'=>$this->Crud->read($ip)->result(),
			);
			//$this->viewdata($data);			
			$this->load->view($this->view,$data);
			//print_r($data['data']);
		}
	}
	public function tabel(){
		$global_set=array(
			'headline'=>false,
			'url'=>$this->default_url,
			'submenu'=>false,
		);
		//LOAD FUNCTION GLOBAL SET
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
		);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->result(),
		);
		//$this->viewdata($data);
		$this->load->view($this->default_view.'tabel',$data);		
	}
	public function grafik($interface=null){
		$interface=str_replace('_', ' ',$interface);
		$ipRouteros = "192.168.50.1";
		$Username=$this->username;
		$Pass=$this->password;
		$api_puerto=8728;
		$interface = $interface; //"<pppoe-nombreusuario>";
		//$interface = 'ether2';
			$API = new routeros_api();
			$API->debug = false;
			if ($API->connect($ipRouteros , $Username , $Pass, $api_puerto)) {
				$rows = array(); $rows2 = array();	
				   $API->write("/interface/monitor-traffic",false);
				   $API->write("=interface=".$interface,false);  
				   $API->write("=once=",true);
				   $READ = $API->read(false);
				   $ARRAY = $API->parse_response($READ);
					if(count($ARRAY)>0){  
						//$rx = number_format($ARRAY[0]["rx-bits-per-second"]/1024,1);
						// $rx = number_format($ARRAY[0]["rx-bits-per-second"]/1048576,0);
						// $tx = number_format($ARRAY[0]["tx-bits-per-second"]/1048576,0);

						// //KONVERSI KE BILANGAN PECAHAN
						// $rx=number_format(str_replace(',', '.', $rx),2);
						// $tx=number_format(str_replace(',', '.', $tx),2);
						$rx=$ARRAY[0]["rx-bits-per-second"]/1048576;
						$tx=$ARRAY[0]["tx-bits-per-second"]/1048576;
						$rx=number_format(str_replace(',', '.', $rx),2);
						$tx=number_format(str_replace(',', '.', $tx),2);						
						
						$rows['name'] = 'Tx';
						$rows['data'][] = $tx;
						$rows2['name'] = 'Rx';
						$rows2['data'][] = $rx;
					}else{  
						echo $ARRAY['!trap'][0]['message'];	 
					} 
			}else{
				echo "<font color='#ff0000'>Koneksi down/gagal, silahkan di cek ulang</font>";
			}
			$API->disconnect();

			$result = array();
			array_push($result,$rows);
			array_push($result,$rows2);
			print json_encode($result, JSON_NUMERIC_CHECK);
	}
	protected function api($query=null){
		$ipRouteros = $this->iprouter;
		$Username=$this->username;
		$Pass=$this->password;
		$api_puerto=$this->api_port;
		//$interface = 'ether2';
		$API = new routeros_api();
		$API->debug = false;
		if($query){
			if ($API->connect($ipRouteros , $Username , $Pass, $api_puerto)) {
			   $API->write('/system/ident/print',true);
			   $READ = $API->read(false);
			   $ARRAY = $API->parse_response($READ);
			   $name = $ARRAY[0]["name"];
			   if(count($ARRAY)>0){
			       $API->write($query['syntak'],true);
			       $READ = $API->read(false);
			       $ARRAY = $API->parse_response($READ);   

			       //$data['uptime']=$ARRAY[0]["uptime"];
			       //$rows['uptime']=$ARRAY[0]["uptime"];
			       
			       return $ARRAY;
				}else{  
			       	echo '<img src="icon_led_grey.png" />&nbsp;'.$ARRAY['!trap'][0]['message'];
			       	exit();	 
			    }
		    }else{
				echo "<font color='#ff0000'>Koneksi down/gagal, silahkan di cek ulang</font>";
				exit();
			}				
		}else{
			echo "Syntak tidak boleh kosong";
			exit();
		}
	}
	public function resource($interface=null){
		$query=array(
			'syntak'=>'/system/resource/print',
		);
		$rows=$this->api($query);
		//print_r($rows);$rx=number_format(str_replace(',', '.', $rx),2);
		$freemem=$rows[0]['free-memory']/1048576;
		$memory=3967-$freemem;
		$data=array(
			'uptime'=>str_replace('w','w ',str_replace('d', 'd ',str_replace('h', 'h ',str_replace('m', 'm ',$rows[0]['uptime'] )))),
			'boardname'=>$rows[0]['board-name'],
			'cpuload'=>$rows[0]['cpu-load'],
			'freehdd'=>number_format(str_replace(',','.',($rows[0]['free-hdd-space']/1048576)),1),
			//'freememory'=>number_format(str_replace(',','.',($rows[0]['free-memory']/1073741824)),1),
			'usedmemory'=>number_format($memory,1),
			//'memory'=>number_format(str_replace(',','.',($rows[0]['free-memory']/1048576)),1),
			
		);
		// $row0['name']='uptime';
		// $row0['data'][]=$data['uptime'];
		$result=array();
		array_push($result,$data);
		//array_push($result,$row1);
		print json_encode($result, JSON_NUMERIC_CHECK);
		//return $this->output->output_
	}
	public function resource2(){
		$ipRouteros="192.168.50.1";  // tu RouterOS.
		$Username=$this->username;
		$Pass=$this->password;
		$api_puerto=8728;
		$API = new routeros_api();
		$API->debug = false;
		if ($API->connect($ipRouteros , $Username , $Pass, $api_puerto)) {
			   $API->write("/system/ident/getall",true);
			   $READ = $API->read(false);
			   $ARRAY = $API->parse_response($READ);
			   $name = $ARRAY[0]["name"];
			   if(count($ARRAY)>0){
	           $API->write("/system/resource/print",true);
	           $READ = $API->read(false);
	           $ARRAY = $API->parse_response($READ);   
	           $nlevel = $ARRAY[0]["uptime"];

	           // echo "Uptime".$nlevel;
	           print_r($ARRAY);
		    }else{  //el servidor API esta of line
		           echo '<img src="icon_led_grey.png" />&nbsp;'.$ARRAY['!trap'][0]['message'];	 
		    }    

		}else{
		    echo "<font color='#ff0000'>La conexion ha fallado. Verifique si el Api esta activo.</font>";
		}
		$API->disconnect();		   		
	}		

}
