/*
 Navicat Premium Data Transfer

 Source Server         : PHPMYADMIN
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : monitoring

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 06/03/2019 15:33:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for barang
-- ----------------------------
DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang`  (
  `barang_id` int(11) NOT NULL,
  `barang_nama` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `barang_tgl_pembelian` date NULL DEFAULT NULL,
  `barang_keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`barang_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ip
-- ----------------------------
DROP TABLE IF EXISTS `ip`;
CREATE TABLE `ip`  (
  `ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_alias` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_address` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_status` binary(1) NULL DEFAULT NULL,
  PRIMARY KEY (`ip_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ip
-- ----------------------------
INSERT INTO `ip` VALUES (1, 'localhost', 'localhost', 0x31);
INSERT INTO `ip` VALUES (2, 'Antiran Poli LT 1 Selatan', '192.168.51.25', 0x31);
INSERT INTO `ip` VALUES (3, 'Antiran Poli LT 1 Tengah', '192.168.51.26', 0x31);
INSERT INTO `ip` VALUES (6, 'Antiran Poli LT 1 Utara', '192.168.51.27', 0x31);
INSERT INTO `ip` VALUES (7, 'Antiran Poli LT 2 Selatan', '192.168.52.28', 0x31);
INSERT INTO `ip` VALUES (8, 'Antiran Poli LT 2 Utara', '192.168.52.8', 0x31);
INSERT INTO `ip` VALUES (9, 'Server Tera ', '192.168.11.110', 0x31);
INSERT INTO `ip` VALUES (10, 'Admisi Lobi 1', '192.168.51.6', 0x31);
INSERT INTO `ip` VALUES (11, 'Admisi Lobi 2', '192.168.51.7', 0x31);
INSERT INTO `ip` VALUES (12, 'Antrian Anak 1', '192.168.51.50', 0x31);
INSERT INTO `ip` VALUES (13, 'Antrian Anak 2', '192.168.51.51', 0x31);
INSERT INTO `ip` VALUES (14, 'Antrian Anak 3', '192.168.51.52', 0x31);
INSERT INTO `ip` VALUES (15, 'Antrian Anak 4', '192.168.51.53', 0x31);
INSERT INTO `ip` VALUES (16, 'Antrian Apotek', '192.168.51.65', 0x31);
INSERT INTO `ip` VALUES (17, 'Antrian Apotek Kasir', '192.168.51.66', 0x30);
INSERT INTO `ip` VALUES (18, 'DVR 1', '192.168.50.64', 0x31);
INSERT INTO `ip` VALUES (19, 'Antrian Dokter 1 Lt 1', '192.168.51.28', 0x31);
INSERT INTO `ip` VALUES (20, 'Server Elims Lab', '192.168.15.2', 0x31);
INSERT INTO `ip` VALUES (21, 'Antrian Dokter 2 Lt 1', '192.168.51.29', 0x31);
INSERT INTO `ip` VALUES (22, 'Antrian Dokter 3 Lt 1', '192.168.51.30', 0x31);
INSERT INTO `ip` VALUES (23, 'Antrian Dokter 4 Lt 1', '192.168.51.31', 0x31);
INSERT INTO `ip` VALUES (24, 'Antrian Dokter 5 Lt 1', '192.168.51.32', 0x31);
INSERT INTO `ip` VALUES (25, 'Antrian Dokter 6 Lt 1', '192.168.51.33', 0x31);
INSERT INTO `ip` VALUES (26, 'Antrian Fisioterapi Lt 1', '192.168.50.49', 0x31);
INSERT INTO `ip` VALUES (27, 'Antrian Radiologi', '192.168.50.18', 0x31);

-- ----------------------------
-- Table structure for kegiatan
-- ----------------------------
DROP TABLE IF EXISTS `kegiatan`;
CREATE TABLE `kegiatan`  (
  `kegiatan_id` int(255) NOT NULL AUTO_INCREMENT,
  `kegiatan_nama` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kegiatan_keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kegiatan_date` date NULL DEFAULT NULL,
  `kegiatan_file` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`kegiatan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kegiatan
-- ----------------------------
INSERT INTO `kegiatan` VALUES (61, 'adwd', 'teloo', '2018-11-24', 'cf025eb64962066f05f249398d1886dd.pdf');
INSERT INTO `kegiatan` VALUES (73, 'hhh', 'hhh', '2018-11-24', NULL);
INSERT INTO `kegiatan` VALUES (74, 'heloo', 'hello', '2018-11-25', '3198a2947b6e1e8433ff5c584bb05c22.pdf');
INSERT INTO `kegiatan` VALUES (77, 'dada', 'dawdawss', '2019-02-25', NULL);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_ikon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_is_mainmenu` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_akses_level` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_urutan` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_status` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'master', 'fa fa-tasks', '0', 'master/admin', '1', '1', '1');
INSERT INTO `menu` VALUES (2, 'barang', 'fa fa-circle-o', '1', 'barang/admin', '1', '1', '1');
INSERT INTO `menu` VALUES (4, 'home', 'fa fa-tasks', '0', 'frontend/home', '0', '2', '0');
INSERT INTO `menu` VALUES (5, 'download', 'fa fa-download', '0', 'frontend/download', '0', '4', '0');
INSERT INTO `menu` VALUES (6, 'kegiatan', 'fa fa-tasks', '0', 'frontend/kegiatan', '0', '3', '0');
INSERT INTO `menu` VALUES (7, 'monitoring', 'fa fa-dashboard', '0', 'frontend/monitoring', '0', '1', '1');
INSERT INTO `menu` VALUES (8, 'dashboard', 'fa fa-circle-o', '7', 'frontend/dashboard', '0', '1', '1');
INSERT INTO `menu` VALUES (9, 'ping', 'fa fa-circle-o', '7', 'frontend/ping', '0', '2', '1');
INSERT INTO `menu` VALUES (10, 'pengaturan', 'fa fa-gears', '0', 'frontend/pengaturan', '0', '99', '1');
INSERT INTO `menu` VALUES (11, 'ip address', 'fa fa-circle-o', '10', 'frontend/ip', '0', '1', '1');
INSERT INTO `menu` VALUES (12, 'trafik', 'fa fa-circle-o', '7', 'frontend/trafik', '0', '2', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `user_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_level` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_terdaftar` date NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', 'admina', '1', '2018-09-29');
INSERT INTO `user` VALUES (3, 'haryanto', 'haryanto', 'haryanto duwi', '2', '2018-10-21');

SET FOREIGN_KEY_CHECKS = 1;
